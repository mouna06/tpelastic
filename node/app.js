const fs = require('fs');
// var elasticsearch = require('elasticsearch')
const csv = require('csv-parser');

const { Client } = require('@elastic/elasticsearch')

const TITLES = [
    'title',
    'seo_title',
    'url',
    'author',
    'date',
    'category',
    'locales',
    'content',
]

const path = process.env.FILE_PATH || './data/data.csv'
//var csv is the CSV file with headers
const esHost = process.env.ESHOST || 'elasticsearch'

var client = new Client({
    node: `http://elastic:changeme@${esHost}:9200`,
    log: 'trace'
});

var results = [];
fs.createReadStream(path)
    .pipe(csv(TITLES))
    .on('data', (data) => results.push({
        ...data,
        date: Date.parse(data.date)
    }))
    .on('end', () => {
        fs.writeFileSync('data/data.json', JSON.stringify(results));

        client.helpers.bulk({
            datasource: results,
            onDocument(doc) {
                return {
                    index: { _index: 'group1' }
                }
            }
        }).then(result => console.log(result))
    });






