const fs = require('fs');

const TITLES = [
    'title',
    'seo_title',
    'url',
    'author',
    'date',
    'category',
    'locales',
    'content',
]

const path = process.env.FILE_PATH


//var csv is the CSV file with headers
function csvJSON(csv) {

    var lines = csv.split("\n");

    var result = [];

    // NOTE: If your columns contain commas in their values, you'll need
    // to deal with those before doing the next step 
    // (you might convert them to &&& or something, then covert them back later)
    // jsfiddle showing the issue https://jsfiddle.net/
    var headers = lines[0].split(";");

    for (var i = 1; i < lines.length; i++) {

        var obj = {};
        var currentline = lines[i].split(",");

        for (var j = 0; j < headers.length; j++) {
            obj[TITLES[j]] = currentline[j];
        }

        result.push(obj);

    }

    //return result; //JavaScript object
    return JSON.stringify(result); //JSON
}


fs.writeFileSync('data/data.json', csvJSON(fs.readFileSync(path, 'utf-8')));